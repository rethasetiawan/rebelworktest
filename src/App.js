import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import MainPage from './containers/MainPage';
import SelectedPage from './containers/SelectedPage';

function App() {
  return (
  <Router >
    <Routes>
      <Route path="/" element={ <MainPage/>} />
      <Route path="/selected" element={ <SelectedPage/>} />
    </Routes>
  </Router>
  );
}

export default App;
