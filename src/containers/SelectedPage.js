import React,{useState, useEffect} from "react";
import { connect } from 'react-redux'
import StarRatings from 'react-star-ratings';
import arrowDown from '../img/arrowDown.png';
import axios from 'axios';
import './MainPage.css'

const SelectedPage = (props) => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);


    const fetchData = async () => {
        try {
          setLoading(true);
          const response = await axios.get(
            `https://api.themoviedb.org/3/movie/${props.movie.id}/similar?api_key=88d55db873d1a876c5c747cd33d76ca5&language=en-US&page=1`
          );
          setData(response.data.results)
          console.log('data',data)
          console.log('res',response)
        } catch (error) {
          console.log('e',error)
        } finally {
          setLoading(false);
        }
      };

    useEffect(() => {
        fetchData();
        }, []); 

    const LoadPoster =() =>{
        fetchData()
        setOpen(true)  
    }

      const PosterSlideshow = (index,num) => {
        return(
          <div key={index} style={{width:300,height:500, marginRight:30}}>
             <div  class="img__wrap"
              style={{ 
              backgroundImage:`url(https://image.tmdb.org/t/p/w300/${data[num].poster_path})`,
              backgroundRepeat: 'no-repeat',
              width:300, 
              height:450,
              display: 'flex',
              justifyContent:'flex-end',
              flexDirection: "column",
              objectFit: "contain"
            }}>
              <div style={{color:'white',margin:20, marginBottom:30}}>
                <div style={{ justifyContent:'flex-end'}}>
                <div style={styles.genreLabel}>{data[num].genre_ids[0]}</div>
                <StarRatings
                    rating={data[num].vote_average/2}
                    starDimension="15px"
                    starSpacing="2px"
                    starRatedColor="white"
                    starEmptyColor="transparent"
                  />
                </div>
                <div style={{fontSize:24, marginTop:10}}>
                  {data[num].original_title}
                </div>
              </div>
  
              <div>
                <div class="img__description_layer">
                  <p  className="img__description">{`Watch now >`} </p>
                </div>
              </div>
            </div>
      
          </div>
        )
      }

      const MovieEpisode = () =>{
        let year = props.movie.release_date.slice(0,4)
    
        return(
            <div style={{display:'flex',justifyContent:'center'}}>
            <div
                style={{ ...styles.posterContainer,
                backgroundImage:`url(https://image.tmdb.org/t/p/w300/${props.movie.poster_path})`,
             }}>
                 <div style={{padding:20}}>
                    <div style={styles.genreLabel}>{props.movie.genre_ids[0]}</div>
                    <StarRatings
                        rating={props.movie.vote_average/2}
                        starDimension="15px"
                        starSpacing="2px"
                        starRatedColor="white"
                        starEmptyColor="transparent"
                        />
                    <span>Release Year : {year} </span>
                    <div style={{fontSize:20,fontWeight:'bold'}}>{props.movie.original_title}</div>
                 </div>
            </div>
    
            <div style={{ display:'flex'}}>
                <div>
                    <div style={{fontSize:32,fontWeight:'bold'}}>Synopsis</div> 
                    <div style={styles.descriptionLabel}>
                        {props.movie.overview}
                    </div>
                    <div style={{flexDirection:'row', display:'flex'}}> 
                        <div style={{fontSize:32,fontWeight:'bold'}}>Episodes</div> 
                        <div style={styles.episodeLabel}>
                            Episode 1
                            <img src={arrowDown} style={{width:20,height:20,marginLeft:10}}/>
                        </div>
                    </div>
                </div>
               <div>
                    <div>Cast</div>
                    <div>Gal Gadot</div>
                    <div style={{color:'yellow'}}>more</div>
               </div>
               
            </div>
    
        </div>
        )
    }
    console.log(props.movie)

    return(
        <div style={{paddingTop:'5%', color:'white',backgroundColor:'#191919'}}>
           {MovieEpisode()}
           

           {open ?
            <div style={styles.otherPosterContainer}>
            <div style={{fontSize:30}}>You Also Might Like This!</div>
             <div style={styles.swiper}>
                {[1,2,3,4,5,6,7,8].map((slideImage, index)=> (
                PosterSlideshow(slideImage,index)
                    ))} 
              </div>          
            </div> :  
                <button style={styles.buttonContainer} onClick={()=>LoadPoster()}>Load</button> 
            }
        </div>
      
    )
}

const styles={
    posterContainer:{
        backgroundRepeat: 'no-repeat',
        width:300, 
        height:450,
        display: 'flex',
        justifyContent:'flex-end',
        flexDirection: "column",
        marginRight:30
    },
    descriptionLabel:{
        width:700, 
        background:'rgba(52, 52, 52, 0.8)',
        opacity:0.4,
        lineHeight:2,
        padding:15,
        marginTop:20,
        marginBottom:20,
        marginRight:20
    },
    genreLabel:{
      background:' rgba(171, 205, 239, 0.3)',
      color:'rgba(79, 211, 196,1)',
      width:50,
      borderRadius:5,
      marginBottom:10,
      alignText:'center',
    },
    swiper:{
        marginTop:20,
        display: 'flex',
        overflowX: 'scroll',
    },
    otherPosterContainer:{
        display:'flex',
        justifyContent:'center', 
        flexDirection:'column',
        paddingRight:'20%',
        paddingLeft:'20%',
        marginTop:20
    }, 
    buttonContainer:{
        color:'yellow',
        border:'1px solid yellow',
        padding:10,
        marginTop:20,
        marginLeft:'20%',
        width:150,
        backgroundColor:'transparent',
        borderRadius:20,
        
      },
      episodeLabel:{
        alignItems:'center',
        display:'flex',
        background:'rgba(52, 52, 52, 0.8)',
        padding:10,
        width:100,
        marginLeft:20
      },
}
const mapStateToProps = state => {
    return {
        movie: state.movie.selectedMovieData
    }
  }
  
  export default connect(mapStateToProps)(SelectedPage)