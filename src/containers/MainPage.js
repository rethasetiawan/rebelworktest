import React,{useState, useEffect} from "react";
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import axios from 'axios';
import { connect } from 'react-redux'
import { getMovieData } from '../redux/Actions/MovieActions';
import StarRatings from 'react-star-ratings';
import { useNavigate } from 'react-router-dom';
import './MainPage.css'

const MainPage = (props) => {
    const [data, setData] = useState([]);
    const [open, setOpen] = useState(false);
    const [page, setPage] = useState(1);

    let navigate = useNavigate();
      // useEffect(() => {
      //   const fetchData = async () => {
      //     try {
      //       setLoading(true);
      //       const response = await axios.get(
      //         `https://api.themoviedb.org/3/movie/now_playing?api_key=88d55db873d1a876c5c747cd33d76ca5&language=en-US&page=${page}`
      //       );
      //       setData((data) => [...data, ...response.data.results]);
      //     } catch (error) {
      //       console.log('e',error)
      //     } finally {
      //       setLoading(false);
      //     }
      //   };

      //   fetchData();
      // }, [page]);

    const fetchData = async () =>{
          try {
            const response = await axios.get(`https://api.themoviedb.org/3/movie/now_playing?api_key=88d55db873d1a876c5c747cd33d76ca5&language=en-US&page=${page}`);
            setData((data) => [...data, ...response.data.results]);
          } catch (error) {
            console.error(error.message);
          }
    }
    
    useEffect(() => {
      fetchData()
      },[page]);
     
    const addNewCompany =() =>{
        fetchData()
        setOpen(true)
    }

    const WatchNowButton = () => {
      return(
        <button style={styles.buttonContainer} >
            <div>Watch Now</div>
        </button>
      )
    }

    const Slideshow = (index,num) => {
      return(
        <div className="each-slide" key={index}>
          <div style={{...styles.backgroundContainer,
              backgroundImage: `url(https://image.tmdb.org/t/p/original/${data[num].backdrop_path})`
           }}>
              <div style={{padding:50,paddingTop:100,color:'white'}}>
                <div style={styles.genreLabel}>{data[num].genre_ids[0]}</div>
                <StarRatings
                  rating={data[num].vote_average/2}
                  starDimension="15px"
                  starSpacing="5px"
                  starRatedColor="white"
                  starEmptyColor="transparent"
                />
                <div style={{fontSize:32, fontWeight:'bold'}}>
                {data[num].original_title}
                </div>
                <div style={{fontSize:12,lineHeight:2, paddingTop:20,width:'50%'}}>
                {data[0].overview}
                </div>
                {WatchNowButton()}
              </div>
          </div>
          </div>
      )
    }
    
    const nextPage = (num) =>{
      console.log('test',data[num])
      props.getMovieData(data[num])
      navigate("/selected")
    }

  const PosterSlideshow = (index,num) => {
      return(
        <div key={index} style={{width:300,height:500, marginRight:30}}>
           <div  class="img__wrap"
            style={{ 
            backgroundImage:`url(https://image.tmdb.org/t/p/w300/${data[num].poster_path})`,
            backgroundRepeat: 'no-repeat',
            width:300, 
            height:450,
            display: 'flex',
            justifyContent:'flex-end',
            flexDirection: "column",
            objectFit: "contain"
          }}>
            <div style={{color:'white',margin:20, marginBottom:30}}>
              <div style={{ justifyContent:'flex-end'}}>
              <div style={styles.genreLabel}>{data[num].genre_ids[0]}</div>
              <StarRatings
                  rating={data[num].vote_average/2}
                  starDimension="15px"
                  starSpacing="2px"
                  starRatedColor="white"
                  starEmptyColor="transparent"
                />
              </div>
              <div style={{fontSize:24, marginTop:10}}>
                {data[num].original_title}
              </div>
            </div>

            <div>
              <div onClick={()=>nextPage(num)}  class="img__description_layer">
                <p  className="img__description">{`Watch now >`} </p>
              </div>
            </div>
          </div>
    
        </div>
      )
    }

    const loadMore = () => {
      setPage((page) => page + 1);
    };

    return(
      <div style={{backgroundColor:'#191919'}}>
        {open ?

        <>
        <Slide easing="ease" indicators>
        {[1,2,3].map((slideImage, index)=> (
            Slideshow(slideImage,index)
            ))} 
        </Slide>

        <div style={{padding:'0px 60px 0px 60px'}}>

          <h2 style={{color:'white'}}>New Release</h2>
              <div style={styles.swiper}>
                {[1,2,3,4,5,6,7,8].map((slideImage, index)=> (
                PosterSlideshow(slideImage,index)
                    ))} 
              </div>
        </div>

        </>
        : 
        <button style={styles.buttonContainer} onClick={()=>addNewCompany()}>Load</button>
      } 
         
      </div>
    )
}

const styles={

    backgroundContainer:{
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        height:500
    },
    posterContainer:{
      // backgroundPosition: 'center',
      // backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      // height:500
  },
    buttonContainer:{
      color:'yellow',
      border:'1px solid yellow',
      padding:10,
      marginTop:20,
      width:150,
      backgroundColor:'transparent',
      borderRadius:20,
    },
    genreLabel:{
      background:' rgba(171, 205, 239, 0.3)',
      color:'rgba(79, 211, 196,1)',
      width:50,
      borderRadius:5,
      marginBottom:10,
      alignText:'center',
    },

    swiper:{
      display: 'flex',
      overflowX: 'scroll',
    }
}
const mapStateToProps = state => {
  return {
      movie: state.movie
  }
}

const mapDispatchToProps = {
    getMovieData
};
export default connect(mapStateToProps,mapDispatchToProps)(MainPage)