import { 
  GET_MOVIE_NP_REQUEST,
  GET_MOVIE_NP_SUCCESS,
  GET_MOVIE_NP_FAILED,
  SELECTED_MOVIE_DATA
 } from '../types';

const getMovieData = (movie) => ({
  type: SELECTED_MOVIE_DATA,
  payload: movie,
});


export { getMovieData };

// const getMovieData = () => async (dispatch) => {
//   dispatch({ type: GET_MOVIE_NP_REQUEST });
//   try {
//     const result = await axios.get('https://api.themoviedb.org/3/movie/now_playing?api_key=88d55db873d1a876c5c747cd33d76ca5&language=en-US&page=1');
//     // console.log('res', result.data)
//     dispatch({ type: GET_MOVIE_NP_SUCCESS, payload: result.data });
//   } catch (error) {
//     dispatch({ type: GET_MOVIE_NP_FAILED });
//   }
// };

// export { getMovieData };
