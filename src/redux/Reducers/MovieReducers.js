import {
   MOVIE_DATA,
  GET_MOVIE_NP_REQUEST,
  GET_MOVIE_NP_SUCCESS,
  GET_MOVIE_NP_FAILED,
  SELECTED_MOVIE_DATA
 } 
  from '../types';
  
  const INITIAL_STATE = {
    movieData:[],
    selectedMovieData:[],
  };
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
     
      case GET_MOVIE_NP_SUCCESS:
          return {
            ...state,
            movieData: action.payload,
          };
      case GET_MOVIE_NP_FAILED:
          return {
            ...state,
            movieData: [],
          };
          case SELECTED_MOVIE_DATA:
            return {
              ...state,
              selectedMovieData:action.payload,
            };
      default:
        return state;
    }
  };
  