import { combineReducers } from 'redux';

import MovieReducers from './MovieReducers';

const rootReducer = combineReducers({
  movie: MovieReducers,
});

export default rootReducer;
