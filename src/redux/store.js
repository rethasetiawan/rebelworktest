// import ReduxThunk from 'redux-thunk';
// import { applyMiddleware, compose, createStore } from 'redux';

// import rootReducer from '../redux/Reducers/index';

// let createAppStore;

// const composeEnhancers =
//   typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
//     ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
//     : compose;

//   createAppStore = applyMiddleware(ReduxThunk);

// export const store = createStore(rootReducer, composeEnhancers(createAppStore));

// export default store;

import {createStore} from 'redux';
import rootReducer from '../redux/Reducers/index';


const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;